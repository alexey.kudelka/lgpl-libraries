"""JSON-RPC entrypoint."""
from typing import List, Optional

import fastapi_jsonrpc as jsonrpc
import pycountry
from toolz import valfilter

from lgpl_libraries.pycountry_api.errors import error_wrapper
from lgpl_libraries.pycountry_api.models import Country, Currency, Language

pycountry_api = jsonrpc.Entrypoint("/lib/pycountry")


@pycountry_api.method(name="countries.list")
@error_wrapper
def list_countries() -> List[Country]:
    """Get list of countries."""
    return [Country.from_orm(country) for country in pycountry.countries]


@pycountry_api.method(name="countries.get")
@error_wrapper
def get_country(
    alpha_2: str = None,
    alpha_3: str = None,
    name: str = None,
    numeric: str = None,
) -> Optional[Country]:
    """Get country by one of kwarg."""
    kwargs = dict(alpha_2=alpha_2, alpha_3=alpha_3, name=name, numeric=numeric)
    kwargs = valfilter(lambda x: x, kwargs)
    country = pycountry.countries.get(**kwargs)
    return Country.from_orm(country) if country else None


@pycountry_api.method(name="countries.search_fuzzy")
@error_wrapper
def country_search(query: str) -> List[Country]:
    """Search countries."""
    countries = pycountry.countries.search_fuzzy(query)
    return [Country.from_orm(country) for country in countries]


@pycountry_api.method(name="countries.lookup")
@error_wrapper
def country_lookup(query: str) -> Optional[Country]:
    """Look up countries case insensitively without knowing which key the value may match."""
    country = pycountry.countries.lookup(query)
    return Country.from_orm(country) if country else None


@pycountry_api.method(name="languages.list")
@error_wrapper
def list_languages() -> List[Language]:
    """Get list of languages."""
    return [Language.from_orm(language) for language in pycountry.languages]


@pycountry_api.method(name="languages.get")
@error_wrapper
def get_language(
    alpha_3: str = None,
    name: str = None,
) -> Optional[Language]:
    """Get language by one of kwarg."""
    kwargs = dict(alpha_3=alpha_3, name=name)
    kwargs = valfilter(lambda x: x, kwargs)
    language = pycountry.languages.get(**kwargs)
    return Language.from_orm(language) if language else None


@pycountry_api.method(name="languages.lookup")
@error_wrapper
def language_lookup(query: str) -> Optional[Language]:
    """Look up languages case insensitively without knowing which key the value may match."""
    language = pycountry.languages.lookup(query)
    return Language.from_orm(language) if language else None


@pycountry_api.method(name="currencies.list")
@error_wrapper
def list_currencies() -> List[Currency]:
    """Get list of currencies."""
    return [Currency.from_orm(currency) for currency in pycountry.currencies]


@pycountry_api.method(name="currencies.get")
@error_wrapper
def get_currency(
    alpha_3: str = None,
    name: str = None,
    numeric: str = None,
) -> Optional[Currency]:
    """Get currency by one of kwarg."""
    kwargs = dict(alpha_3=alpha_3, name=name, numeric=numeric)
    kwargs = valfilter(lambda x: x, kwargs)
    currency = pycountry.currencies.get(**kwargs)
    return Currency.from_orm(currency) if currency else None


@pycountry_api.method(name="currencies.lookup")
@error_wrapper
def currency_lookup(query: str) -> Optional[Currency]:
    """Look up currencies case insensitively without knowing which key the value may match."""
    currency = pycountry.currencies.lookup(query)
    return Currency.from_orm(currency) if currency else None
