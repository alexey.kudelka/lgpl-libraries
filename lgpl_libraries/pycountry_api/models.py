"""Pycountry data models."""
from typing import Optional

from pydantic import BaseModel


class PycountryBase(BaseModel):
    """Base pycontry object representation."""

    class Config:
        orm_mode = True


class Country(PycountryBase):
    """Country representation."""

    alpha_2: str
    alpha_3: str
    name: str
    numeric: str
    official_name: Optional[str]


class Language(PycountryBase):
    """Language representation."""

    alpha_3: str
    name: str
    scope: str
    type: str


class Currency(PycountryBase):
    """Currency representation."""

    alpha_3: str
    name: str
    numeric: str
