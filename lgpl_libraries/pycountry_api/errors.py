"""JSON-RPC errors."""
from functools import wraps

from fastapi_jsonrpc import BaseError


class PycountryLookupError(BaseError):
    """LookupError."""

    CODE = 5000
    MESSAGE = "Lookup Error"


class PycountryTypeError(BaseError):
    """TypeError."""

    CODE = 5001
    MESSAGE = "Type Error"


def error_wrapper(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except TypeError:
            raise PycountryTypeError
        except LookupError:
            raise PycountryLookupError
    return decorator
