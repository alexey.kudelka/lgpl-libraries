"""Application entrypoint."""
import logging.config  # noqa: WPS301

import fastapi_jsonrpc as jsonrpc
import uvicorn
from fastapi.responses import ORJSONResponse

from lgpl_libraries import settings
from lgpl_libraries.pycountry_api.entrypoint import pycountry_api
from lgpl_libraries.settings import config

logging.config.dictConfig(settings.LOGGING)


app = jsonrpc.API(
    docs_url=None if config.HIDE_SWAGGER else "/docs",
    redoc_url=None if config.HIDE_SWAGGER else "/redoc",
    openapi_url=None if config.HIDE_SWAGGER else "/openapi.json",
    default_response_class=ORJSONResponse,

)

app.bind_entrypoint(pycountry_api)

if __name__ == "__main__":  # pragma: no cover
    uvicorn.run(
        'lgpl_libraries.main:app',
        port=config.PORT,
        reload=True,
        log_config=None,
    )
