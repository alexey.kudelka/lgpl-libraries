"""Project settings."""
from pathlib import Path

from environs import Env
from pydantic import BaseSettings

BASE_DIR: Path = Path(__file__).parent.parent
Env.read_env()


class Config(BaseSettings):
    """Application settings."""

    DEBUG: bool = False

    HIDE_SWAGGER: bool = False
    PORT: int = 8011
    LOG_LEVEL: str = "INFO"

    class Config:  # noqa: WPS306, WPS431 configuration for settings class
        env_prefix = "LGPL_"


config = Config()


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s %(levelname)s %(name)s:%(lineno)d: %(message)s",
        },
        "json": {
            "format": "%(levelname)s %(asctime)s %(pathname)s:%(lineno)d %(process)d %(message)s",
            "()": "pythonjsonlogger.jsonlogger.JsonFormatter",
            "json_ensure_ascii": False,
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": f"{config.LOG_LEVEL.upper()}",
            "formatter": "simple" if config.DEBUG else "json",
            "stream": "ext://sys.stdout",
        },
    },
    "loggers": {
        "": {
            "level": f"{config.LOG_LEVEL.upper()}",
            "handlers": [
                "console",
            ],
        },
    },
}

