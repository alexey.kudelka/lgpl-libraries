FROM python:3.8

ADD . /lgpl-libraries

RUN pip install poetry \
    && poetry config virtualenvs.create false \
    && cd lgpl-libraries \
    && poetry install --no-interaction --no-ansi

WORKDIR /lgpl-libraries
